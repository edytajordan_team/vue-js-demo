import { createApp } from 'vue';
import router from './router';
import store from './store';

//todo tailwind
import '@/assets/css/tailwind.css'

//import components
import App from './App.vue';
import BaseCard from "./components/UI/BaseCard";
import BaseButton from "./components/UI/BaseButton";
import BaseModal from "./components/UI/BaseModal";
import BaseSpinner from "./components/UI/BaseSpinner";

const app = createApp(App);
app.component('base-card', BaseCard);
app.component('base-button', BaseButton);
app.component('base-modal', BaseModal);
app.component('base-spinner', BaseSpinner);

app.use(router);
app.use(store);
app.mount('#app');