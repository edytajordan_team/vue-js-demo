export default {
    async contactCoach(context, payload){
        //console.log('payload', payload);
        const coachMessage = {
            //id: new Date().toISOString(), //todo id will now come from database
            coachId: payload.coachId,
            userEmail: payload.email,
            message: payload.message,
        }

        const response  = await fetch(`${process.env.VUE_APP_FIREBASE_API_URL}/requests/${payload.coachId}.json`, {
            method: 'POST',
            body: JSON.stringify(coachMessage),
        })

        const responseData = await response.json();
        //console.log(responseData);
        if(!response.ok){
            console.log('error from contactCoach');
            const error = new Error(responseData.message || 'Failed to send message.');
            throw error;
        }

        context.commit('addRequest', {
            ...coachMessage,
            id: responseData.name,
        })
    },
    async fetchRequests(context){

        const coachId = context.rootGetters.userId;
        const token = context.rootGetters.token;
       // console.log(`FROM REQUESTS: coach id: ${coachId}, token: ${token}`);

        const response  = await fetch(`${process.env.VUE_APP_FIREBASE_API_URL}/requests/${coachId}.json?auth=${token}`)

        const responseData = await response.json();
        //console.log(responseData);
        if(!response.ok){
            console.log('error from fetchRequests:', responseData);
            const error = new Error(responseData.message || 'Failed to fetch messages.');
            throw error;
        }
        //
        const requests = [];

        for(const key in responseData){
            const request = {
                id: key,
                coachId: coachId,
                userEmail: responseData[key].userEmail,
                message: responseData[key].message,
            }

            requests.push(request);
        }

        context.commit('setRequests', requests);
    },
}