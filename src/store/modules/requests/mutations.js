export default {
    //todo remember this will have an action in actions.js
    addRequest(state, payload){
        state.requests.push(payload);
    },
    setRequests(state, payload){
        state.requests = payload;
    }
}