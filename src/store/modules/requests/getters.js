export default {
    requests(state, getters, rootState, rootGetters){  //todo we can use root state and getters in getters.js
        const coachId = rootGetters.userId;
        //console.log(state.requests);
        return state.requests.filter( request => request.coachId === coachId );
    },
    hasRequests(state, getters){
        //return state.requests && state.requests.length > 0;
        return getters.requests && getters.requests.length > 0;
    }
}