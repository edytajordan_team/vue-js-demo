//todo let's add a global variable to make sure we only have one timer running at a time
// eslint-disable-next-line no-unused-vars
let timer;
import axios from 'axios';

export default {
    async signup(context, payload){
        return context.dispatch('auth', {
            ...payload,
            mode: 'signup',
        })
    },
    async login(context, payload){
        return context.dispatch('auth', {
            ...payload,
            mode: 'login',
        })
    },
    async auth(context, payload){
        const mode = payload.mode;
        let url    = process.env.VUE_APP_FIREBASE_AUTH_LOGIN_URL;

        if(mode == 'signup'){
            url = process.env.VUE_APP_FIREBASE_AUTH_SIGNUP_URL;
        }

        //console.log('url', url);
        //todo FETCH
        // const response  = await fetch(url, {
        //     method: 'POST',
        //     body: JSON.stringify({ //todo always stringify for firebase?
        //         email: payload.email,
        //         password: payload.password,
        //         returnSecureToken: true,
        //     })
        // });
        // const responseData = await response.json();
        // if(!response.ok){
        //     console.log('error from login', responseData);
        //     const error = new Error(responseData.error.message || 'Failed to authenticate. Please try again.');
        //     throw error;
        // }
        //console.log('success -auth ACTION ', responseData);

        //todo AXIOS
        const responseData  = await axios.post(url, {
            //todo always stringify for firebase?
            email: payload.email,
            password: payload.password,
            returnSecureToken: true,
        })
            .catch(error => {
                //console.log('error from login AXIOS', error);
                const eError = new Error(error || 'Failed to authenticate. Please try again.');
                throw eError;
            })

        //TODO NEED TO ADD VALUES INTO VARS FOR BETTER CODE MNGT

        //todo persist token data in local storage
        localStorage.setItem('token', responseData.data.idToken);
        localStorage.setItem('userId', responseData.data.localId);

        //todo process expiration token
        const expiresIn = +responseData.data.expiresIn * 1000; //todo + at the beginning will convert it to number. *1000 will produce mili-seconds to use in Date() method
        //const expiresIn = 5000;
        const expirationDate = new Date().getTime() + expiresIn;
        localStorage.setItem('tokenExpiration', expirationDate);

        //todo let's add timeout function that will check how much time passed and log user out automatically
        timer = setTimeout(function(){
            context.dispatch('autoLogout');
        }, expiresIn);

        //todo we use the same mutation because we get the same data back
        context.commit('setUser', {
            token: responseData.data.idToken,
            userId: responseData.data.localId,
            //tokenExpiration: expirationDate, //todo do we really need this in Vuex?
        });
    },
    tryLogin(context){
        const token  = localStorage.getItem('token');
        const userId = localStorage.getItem('userId');
        const tokenExpiration = localStorage.getItem('tokenExpiration');

        //todo when user logs in, we need to start the timer to check how much time has passed since login to log them out
        const expiresIn = +tokenExpiration - new Date().getTime();
        if(expiresIn < 0){
            return;
        }
        timer = setTimeout(function(){
            context.dispatch('autoLogout');
        }, expiresIn);

        if(token && userId){
            context.commit('setUser', {
                token: token,
                userId: userId,
                //tokenExpiration: tokenExpiration, //todo do we really need this in Vuex?
            })
        }
    },
    logout(context){
        localStorage.removeItem('token');
        localStorage.removeItem('userId');
        localStorage.removeItem('tokenExpiration');

        clearTimeout(timer);

        context.commit('setUser', {
            token: null,
            userId: null,
        });
    },
    autoLogout(context){
        context.dispatch('logout');
        context.commit('didAutoLogout');
    }
}