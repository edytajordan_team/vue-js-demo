export default {
    userId(state){
        return state.userId;
    },
    token(state) {
        //console.log('token from auth getter', state.token);
        return state.token;
    },
    isAuthenticated(state){
        return !!state.token; //todo adding !! will convert to boolean
    }
}