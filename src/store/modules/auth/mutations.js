export default {
    //todo we use the same mutation because we get the same data back
    setUser(state, payload){
        state.token = payload.token;
        state.userId = payload.userId;
        //state.tokenExpiration = payload.tokenExpiration;
    },
    didAutoLogout(state){
        state.didAutoLogout = true;
    }
}