import mutations from './mutations';
import actions from './actions';
import getters from './getters';

export default {
    namespaced: true,
    state(){
        return{
            // coaches: [
            //     {
            //         id: 'c1',
            //         firstName: 'Max',
            //         lastName: 'Terrel',
            //         areas: ['frontend', 'backend', 'career'],
            //         description:
            //             "I'm Maximilian and I've worked as a freelance web developer for years. Let me help you become a developer as well!",
            //         hourlyRate: 35,
            //         pic: 'https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
            //     },
            //     {
            //         id: 'c2',
            //         firstName: 'Julie',
            //         lastName: 'Jones',
            //         areas: ['frontend', 'career'],
            //         description:
            //             'I am Julie and as a senior developer in a big tech company, I can help you get your first job or progress in your current role.',
            //         hourlyRate: 30,
            //         pic: 'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
            //     }
            // ],
            coaches: [

            ],
        }
    },
    mutations,
    getters,
    actions,
}