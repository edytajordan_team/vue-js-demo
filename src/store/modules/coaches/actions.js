export default {
    //todo add async keyword
    async registerCoach(context, payload){
        const userId = context.rootGetters.userId;
        const token = context.rootGetters.token;

        const coachData = {
            firstName: payload.first,
            lastName: payload.last,
            hourlyRate: payload.rate,
            description: payload.desc,
            areas: payload.areas,
            pic: payload.pic,
        }

        //todo use await instead of .then()
        const response = await fetch(`https://vue-http-demo-70033-default-rtdb.firebaseio.com/coaches/${userId}.json?auth=${token}`, {
            method: 'PUT', //todo this will update, not create a new user/coach (temp solution until authentication is in place)
            body: JSON.stringify(coachData),
        }).then();

        const responseData = await response.json();
        if(!response.ok){
            //console.log('error from registerCoach');
            const error = new Error(responseData.message || 'Failed to register coach.');
            throw error;
        }

        //console.log(coachData);
        context.commit('registerCoach', {
            ...coachData,
            id: userId,
        })
    },
    async loadCoaches(context){
        //console.log('dispatched coaches');
        //const userId = context.rootGetters.userId;
        const response = await fetch(`https://vue-http-demo-70033-default-rtdb.firebaseio.com/coaches.json`);

        const responseData = await response.json();
        //console.log(responseData);
        if(!response.ok){
            //console.log('error from loadCoaches');
            const error = new Error(responseData.message || 'Failed to load list of coaches.');
            throw error;
        }

        //todo format data so it is an array and not an object
        const coaches = [];
        //todo need to correctly format coaches array
        for(const key in responseData){
            const coach = {
                firstName: responseData[key].firstName,
                lastName: responseData[key].lastName,
                hourlyRate: responseData[key].hourlyRate,
                description: responseData[key].description,
                areas: responseData[key].areas,
                pic: responseData[key].pic,
                id: key,
            }
            coaches.push(coach);
        }

        context.commit('setCoaches', coaches);
    }

}