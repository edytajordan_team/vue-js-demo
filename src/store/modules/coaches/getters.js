export default {
    coaches(state){
        return state.coaches;
    },
    hasCoaches(state)
    {
        return state.coaches && state.coaches.length > 0;
    },
    isCoach(state, getters, rootState, rootGetters){ //todo each method in getters receives state, getters and root stuff as its params for your advantage
        const coaches = getters.coaches;
        const userId = rootGetters.userId;

        return coaches.some(coach => coach.id === userId); //todo some()
    }
}