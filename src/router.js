import { createRouter, createWebHistory } from "vue-router";
import store from './store'

//components
import Dashboard from "./components/dashboard/Dashboard";
import MonsterSlayer from "./components/monster-slayer/MonsterSlayer";
import IdeaGeneratorApp from "./components/idea-generator-app/IdeaGeneratorApp";
//coaches app components
import CoachesLanding from "./components/pages/find-coach-app/coaches/CoachesLanding";
import CoachDetail from "./components/pages/find-coach-app/coaches/CoachDetail";
import CoachRegistration from "./components/pages/find-coach-app/coaches/CoachRegistration";
import ContactCoach from "./components/pages/find-coach-app/requests/ContactCoach";
import RequestsReceived from "./components/pages/find-coach-app/requests/RequestsReceived";
//
import UserAuth from "./components/auth/UserAuth";
//
import NotFound from "./components/pages/NotFound";

//
//todo remove later
//import Router2Test from "./components/test/Router2Test";
//import GridParent from "./components/datatables/GridParent";
//import TheForm from "./components/forms/TheForm";
// import TheResources from "./components/learning-resources/TheResources";
// import TeamsList from "./components/teams/TeamsList";
// import TeamMembers from "./components/teams/TeamMembers";
//
const router = createRouter({
    history: createWebHistory(),
    //uid: 'router',
    routes: [
        {
            path: '/',
            component: Dashboard,
            name: 'Dashboard',
            meta: {
                showInMainMenu: true,
                icon: 'HomeIcon',
            }
        },
        {
            path: '/monster-slayer-game',
            component: MonsterSlayer,
            name: 'Monster Slayer Game',
            meta: {
                showInMainMenu: true,
                icon: 'CubeTransparentIcon',
            }
        },
        {
            path: '/coaches',
            component: CoachesLanding,
            name: 'Find a Coach App',
            meta: {
                showInMainMenu: true,
                icon: 'UserGroupIcon',
            },
            children: [

            ],
        },
        {
            path: '/coaches/:id/',
            props: true, //todo passing params as props so that component is reusable anywhere and not just through routing
            component: CoachDetail,
            name: 'View Coach',
            meta: {
                showInMainMenu: false,
                icon: null,
            },
            children: [
                {
                    path: 'contact', //todo  /coaches/:coach-id/contact
                    component: ContactCoach,
                },
            ],
        },
        {
            path: '/register',
            component: CoachRegistration,
            name: 'Register',
            meta: {
                showInMainMenu: false,
                icon: null,
                requiresAuth: true,
            }
        },
        {
            path: '/auth',
            component: UserAuth,
            name: 'Auth',
            meta: {
                showInMainMenu: false,
                icon: null,
                requiresUnauth: true,
            }
        },
        {
            path: '/requests',
            component: RequestsReceived,
            name: 'Requests',
            meta: {
                showInMainMenu: false,
                icon: null,
                requiresAuth: true,
            }
        },
        {
            path: '/idea-generator-app',
            component: IdeaGeneratorApp,
            name: 'Idea Generator App',
            meta: {
                showInMainMenu: true,
                icon: 'LightBulbIcon',
            }
        },
        //todo end of remove later
        // {
        //     path: '/ui-components',
        //     component: UIComponents,
        //     name: 'UIComponents',
        //     meta: {
        //         showInMainMenu: true,
        //         icon: 'DocumentTextIcon',
        //     }
        // },
        //
        // {
        //     path: '/bookmarks-app',
        //     component: TheResources,
        //     name: 'Bookmarks App',
        //     meta: {
        //         showInMainMenu: false,
        //         icon: 'BookmarkIcon',
        //     }
        // },
        // {
        //     path: '/stock-trader-app',
        //     component: Router2Test,
        //     name: 'Stock Trader App',
        //     meta: {
        //         showInMainMenu: false, //todo make true!
        //         icon: 'CurrencyDollarIcon',
        //     }
        // },
        // {
        //     path: '/simple-grid',
        //     component: GridParent,
        //     name: 'Simple Grid',
        //     meta: {
        //         showInMainMenu: true,
        //         icon: 'DocumentTextIcon',
        //     }
        // },
        // {
        //     path: '/teams/',
        //     component: TeamsList,
        //     name: 'Teams (Cards Demo)',
        //     meta: {
        //         showInMainMenu: false,
        //         icon: 'UsersIcon',
        //     }
        // },
        // {
        //     path: '/teams/:teamId',
        //     component: TeamMembers,
        //     name: 'Teams (ID)',
        //     props: true, //todo passing params as props so that component is reusable anywhere and not just through routing
        //     meta: {
        //         showInMainMenu: false,
        //     }
        // },
        // {
        //     path: '/query-params',
        //     name: 'Query Params',
        //     components: {
        //         default: Dashboard,
        //         footer: TheResources,
        //     },
        //     meta: {
        //         showInMainMenu: false,
        //     }
        // },
        //todo NOT FOUND (always the last in the menu)
        {
            path: '/:notFound(.*)',
            component: NotFound,
            name: 'Not Found',
            //component: NotFound, //todo create Not Found page
            //redirect: '/',
            meta: {
                showInMainMenu: false,
            }
        }
    ],
});

router.beforeEach(function(to, _, next){
    if(to.meta.requiresAuth && !store.getters.isAuthenticated){ //todo you can import store anywhere
        //next(false); //todo cancels auth
        next('/auth');
    }
    else if(to.meta.requiresUnauth && store.getters.isAuthenticated){
        next('/coaches');
    }
    else{
        next();
    }
});

export default router;